const express = require('express');
const mongoose = require('mongoose');
const url = 'mongodb://127.0.0.1:27017/learning';
const cookieParser = require('cookie-parser');
const {requireAuth, checkUser} = require('./middleware/authMiddleware');


const app = express();

//middleware
app.use(express.static(__dirname + 'public'));
app.use(express.json());
app.use(cookieParser());


//view engines
app.set('view engine', 'ejs')

mongoose.connect(url, {useNewUrlParser:true});

const con = mongoose.connection;

app.use(express.json());

const learningRouters = require('./routes/learning');
app.use('/learning', learningRouters);

const authRouters = require('./routes/authRoutes');
app.use('/users', authRouters);


con.on('open', () => {
    console.log("DB Connected !!!");
});

app.listen(9000, () => {
    console.log("Server Started")
});

//routes
app.get('*',checkUser);
app.get('/', (req, res) => res.render('home'));
app.get('/smoothies', requireAuth ,(req, res) => res.render('smoothies'));
app.use(authRouters);

