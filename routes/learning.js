const express = require("express");
const router = express.Router();
const Learning = require("../models/learn");

router.get("/", async (req, res) => {
  try {
    const learnings = await Learning.find();
    res.json(learnings);
  } catch (error) {
    res.send("Error" + error.message);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const learning = await Learning.findById(req.params.id);
    res.json(learning);
  } catch (error) {
    res.send("Error" + error.message);
  }
});

router.post("/", async (req, res) => {
  const learning = new Learning({
    name: req.body.name,
    tech: req.body.tech,
    sub: req.body.sub,
  });

  try {
    const l1 = await learning.save();
    res.json(l1);
  } catch (error) {
    res.send("Error " + error.message);
  }
});

router.patch("/:id", async (req, res) => {
  try {
    const learning = await Learning.findById(req.params.id);
    learning.tech = req.body.tech;
    const l1 = await learning.save();
    res.json(l1);
  } catch (error) {
    res.send("Error " + error.message);
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const learning = await Learning.findByIdAndRemove(req.params.id);
    const l1 = await learning.remove();
    res.json(l1);
  } catch (error) {
    res.send("Error " + error.message);
  }
});

module.exports = router;
